import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Routes from './Routes'


const App = props => {
    return (
        <Router>
            <Switch>
                {
                    Routes.map((route, key) => (
                        <Route key={key} {...route}/>  
                    ))
                }
            </Switch>
        </Router>
    )
}

export default App