import Home from './Views/Home'
import About from './Views/About'
import Services from './Views/Services'
import Contact from './Views/Contact'
import Detail from './Views/ServiceDetail'

const Routes = [
    {
        path: '/',
        component: Home,
        exact: true
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/services',
        component: Services,
        exact: true
    },
    {
        path: '/services/:slug',
        component: Detail
    },
    {
        path: '/contact',
        component: Contact
    },
]

export default Routes