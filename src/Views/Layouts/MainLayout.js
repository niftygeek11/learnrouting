import React from 'react'
import Header from '../Partials/Header'
import Footer from '../Partials/Footer'

export default props => {
    return (
        <>
            <Header/>
            <main>
                {props.children}
            </main>
            <Footer/>
        </>
    )
}
