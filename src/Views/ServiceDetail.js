import React from 'react'
import MainLayout from './Layouts/MainLayout';

export default props => {
    return (
        <MainLayout>
            <h1>This is detail of {props.match.params.slug}</h1>
        </MainLayout>
    )
}
