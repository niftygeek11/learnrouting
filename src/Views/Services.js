import React from 'react'
import MainLayout from './Layouts/MainLayout';
import {Link} from 'react-router-dom'

export default props => {
    return (
        <MainLayout>
            <h1>This is services</h1>
            <ul>
                <li><Link to="/services/web-designing">Web Designing</Link></li>
                <li><Link to="/services/web-development">Web Development</Link></li>
                <li><Link to="/services/mobile-app-development ">Mobile App Development</Link></li>
                <li><Link to="/services/graphics-designing">Graphics Designing</Link></li>
            </ul>
        </MainLayout>
    )
}
