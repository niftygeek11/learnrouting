import React from 'react'
import {Link} from 'react-router-dom'
import '../Styles/Header.css'

export default props => {
    return (
        <header>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/about">About</Link></li>
                <li><Link to="/services">Services</Link></li>
                <li><Link to="/contact">Contact</Link></li>
            </ul>
        </header>
    )
}
